#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

component_name="${1}"
component_version="${2}"
component_version_file="${3}"
target_branch="${4}"
project="${5}"

function usage() {
  >&2 echo "usage: $0 [component-name] [component-version] [version-file] [target_branch] [project]"
  exit 1
}

if [[ -z "${component_name}" ]] || \
  [[ -z "${component_version}" ]] || \
  [[ -z "${component_version_file}" ]] || \
  [[ -z "${target_branch}" ]] ||
  [[ -z "${project}" ]]; then
  usage
fi

if [[ "${CI_PROJECT_URL}" != "${project}" ]]; then
  # If we're running this on a fork, or a different project, silently exit
  # This is important for security patches
  >&2 echo "Running at a non-standard project location (${CI_PROJECT_URL}). Dependabot will stand-down."
  # Exit without treating this as an error
  exit 0
fi

if [[ -z "${GITLAB_DEPENDABOT_TOKEN:-}" ]]; then
  >&2 echo "No token configured. Dependabot will not execute."
  # Exit without treating this as an error
  exit 0
fi

function generate_changelog_for_iid() {
  cat << EOF
---
title: Upgrade ${component_name} to ${component_version}
merge_request: ${1}
author:
type: other
EOF
}

function create_branch() {
  curl --fail \
    "https://gitlab.com/api/v4/projects/gitlab-org%2fgitlab-ce/repository/branches?branch=${component_name}-update-${component_version}&ref=${target_branch}" \
    --request POST \
    --header "PRIVATE-TOKEN: ${GITLAB_DEPENDABOT_TOKEN}"
}

function update_version_file() {
  curl --fail \
    "https://gitlab.com/api/v4/projects/gitlab-org%2fgitlab-ce/repository/files/${component_version_file}" \
    --request PUT \
    --header "PRIVATE-TOKEN: ${GITLAB_DEPENDABOT_TOKEN}" \
    --header "Content-Type: application/json" \
  --data @- << EOF
{
  "branch": "${component_name}-update-${component_version}",
  "content": "${component_version}",
  "commit_message": "Upgrade ${component_name} to ${component_version}"
}
EOF
}

function create_merge_request() {
  curl --fail \
    "https://gitlab.com/api/v4/projects/gitlab-org%2fgitlab-ce/merge_requests" \
    --header "PRIVATE-TOKEN: ${GITLAB_DEPENDABOT_TOKEN}" \
    --header "Content-Type: application/json" \
  --data @- << EOF | jq '.iid'
{
  "source_branch": "${component_name}-update-${component_version}",
  "target_branch": "${target_branch}",
  "labels": "backstage",
  "title": "Upgrade ${component_name} to ${component_version}",
  "description":  "Upgrade ${component_name} to ${component_version}"
}
EOF
}

function add_changelog() {
  changelog_file="$(generate_changelog_for_iid "${1}"|jq -sR '.')"

  curl --fail \
  "https://gitlab.com/api/v4/projects/gitlab-org%2fgitlab-ce/repository/files/changelogs%2funreleased%2f${component_name}-update-${component_version}.yml" \
  --header "PRIVATE-TOKEN: ${GITLAB_DEPENDABOT_TOKEN}" \
  --header "Content-Type: application/json" \
  --data @- << EOF
{
  "branch": "${component_name}-update-${component_version}",
  "content": ${changelog_file},
  "commit_message": "Add changelog for ${component_name} upgrade to ${component_version}"
}
EOF
}

create_branch

update_version_file

merge_request_iid=$(create_merge_request)

add_changelog "${merge_request_iid}"
