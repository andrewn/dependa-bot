# dependa-bot

A tiny CI bot for managing GitLab dependencies.

# How to use

Include the following in your `.gitlab-ci.yml` file:

```yaml
upgrade-gitlab:
  stage: final
  image: "registry.gitlab.com/andrewn/dependa-bot:dependa-bot"
  script:
    - dependa-bot \
      "FRIENDLY_NAME" \
      "$(git describe --tags|sed 's/^v//')" \
      "GITLAB_VERSION_FILENAME" \
      "master" \
      "https://gitlab.com/gitlab-org/project-path"
  only: tags
```

## Notes

1. This should only run on releases, hence `only: tags`.
1. `FRIENDLY_NAME` is used in MR as descriptive text. For example, `gitlab-workhorse` or `gitaly`.
1. `GITLAB_VERSION_FILENAME` is the name of the file in the GitLab-CE repository which stores the version number.
One of `GITALY_SERVER_VERSION`, `GITLAB_PAGES_VERSION`, `GITLAB_SHELL_VERSION` or `GITLAB_WORKHORSE_VERSION`.
1. `master` refers to the branch the MR will be made against. (Usually `master`)
1. `https://gitlab.com/gitlab-org/project-path` is the URL of the project (eg https://gitlab.com/gitlab-org/gitlab-workhorse).
   At runtime, dependa-bot will check if this matches. If it doesn't, it could be because the project is running
   on a private fork, and the bot will skip creation of the MR.

-----------------------

Spawned from https://gitlab.com/gitlab-org/gitlab-ce/issues/54299#note_147433678
