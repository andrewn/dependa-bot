FROM alpine:latest

RUN apk add --no-cache curl jq bash git
WORKDIR /app
COPY bin/dependa-bot dependa-bot
RUN chmod 755 /app/dependa-bot && ln -s /app/dependa-bot /bin/dependa-bot
